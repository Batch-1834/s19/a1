console.log("Hello World!");
/*










14. Create a git repository named S19.
15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
16. Add the link in Boodle.
*/

// 3. Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
let num = 2;

const getCube = 2 ** 3;

// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
console.log(`The cube of ${num} is: ${getCube}`);

//5. Create a variable address with a value of an array containing details of an address.

const address = [354, "Highway-road", "Manila", "Philippines"];

// // 6. Destructure the array and print out a message with the full address using Template Literals.

console.log(`I live at ${address[0]} ${address[1]}, ${address[2]}, ${address[3]}`);

// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.

const animal = {
	type: "shih",
	kingdom: "Panda",
	weight: 300,
	size: 6,
};
// 8. Destructure the object and print out a message with the details of the animal using Template Literals.
const {type, kingdom, weight, size} = animal;

console.log(`${type} was a ${kingdom}. He weighed at ${weight} pounds and measures at ${size}ft tall`);

// 9. Create an array of numbers.

let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
numbers.forEach((number) => {
	console.log(`${number}`);
})

// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

let reducedNumber = numbers.reduce(function(acc, cur){
        return acc + cur;
    })
    console.log(`${reducedNumber}`);

// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.

class dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

// 13. Create/instantiate a new object from the class Dog and console log the object.
const myDog = new dog();

console.log(myDog);

// Initialized/assigend a value in our object
myDog.name = "Kob-kob";
myDog.age = 2;
myDog.breed = "shih-Poo";
	
console.log(myDog);

